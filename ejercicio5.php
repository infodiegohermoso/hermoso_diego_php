<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title> Ejercicio 5</title>
  </head>
  <body>

    <?
    $numero = 1; #Declaro la variable numero con el valor 1
    print "El valor de numero es: ".$numero."</BR>"; # Imprimo la variable numero
    $numero = 2; # Cambio el valor de la variable numero declarada anteriormente a 2
    print "El valor de numero es: ".$numero."</BR>"; # Imprimo la varibale numero pero nos dara como resultado otro valor.



    function numerosVariables() # Creo la funcion "numerosVariables"
    {
      # Creo una variable que solo estara disponible dentro de la funcion aunque tenga el mismo nombre que la variable declarada anteiormente.
      $numero = 5;
      print "El valor de numero dentro de la funcion es: ".$numero."</BR>";# Imprimo la variable creada en la funcion que no es la misma que la creada anteriormente


    }

    numerosVariables(); #Se ejecuta el codigo dentro de la funcion "numerosVariables"
    print "El valor de numero es: ".$numero."</BR>"; #Imprimo la variable numero y se comprobara que su valor sigue siendo 2




    ?>

  </body>
</html>
