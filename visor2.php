<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title> Ejercicios</title>
  </head>
  <body>
    <h1>Formulario Ejercicio 10</h1>

    <?
     print ("Nombre del Alumno: ".$_GET['nombreAlumno']."<br>"); # Imprimimos el nombre del alumno que haya introducido
     print ("Apellido del Alumno: ".$_GET['apellidoAlumno']."<br>"); # Imprimimos el apellidoAlumno
     print("Asignatura favorita:".$_GET['curso']."<br>"); #Imprimimos la asignatura seleccionada del alumno
     print("Idiomas: "); # Imprimimos el string idiomas

     # Condicionas para comprobar el lenguaje seleccionado
     if (isset($_GET['ingles']))
     {
        echo $_GET['ingles'];
     }
     if (isset($_GET['frances']))
     {
        echo $_GET['frances'];
     }
     if (isset($_GET['español']))
     {
        echo $_GET['español'];
     }
    ?>

  </body>
</html>
