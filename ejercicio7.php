<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title> Ejercicio 7</title>
  </head>
  <body>
    <?

           $color="rojo";

           $$color=" es mi color preferido";


           print ( "El color ".$color. $$color ."<br>");

           print ( "El color ".$color. ${$color}."<br>");

           print ( "El color ".$color. $rojo."<br>");



           print ("Las tres líneas anteriores deben decir lo mismo<br>");
           print ("Hemos invocado la misma variable de tres formas diferentes<BR>");



           $color="magenta";



           print ("Ahora la variable $color ha cambiado a magenta<br>");
           print ("pero como no hemos creado ninguna variable con ese color<br>");
           print ("en las lineas siguientes no aparecerá nada <br>");
           print ("detrás de la palabra magenta <br>");


           print (" El color ".$color.$$color."<br>");
           print (" El color ".$color.${$color}."<br>");


           print ("Pese a que $color vale ahora ".$color."<br>");
           print ("la vieja variable $rojo sigue existiendo <br>");
           print ("y conserva su valor. Es este: ".$rojo);

           # Aqui he comprobado que si hay diferencias con el ejemplo del anterior ejercicio.

?>

  </body>
</html>
